default['praefect']['enable'] = false
default['praefect']['dir'] = "/var/opt/gitlab/praefect"
default['praefect']['log_directory'] = "/var/log/gitlab/praefect"
default['praefect']['listen_addr'] = "localhost:2305"
default['praefect']['prometheus_listen_addr'] = "localhost:9652"
default['praefect']['logging_level'] = nil
default['praefect']['logging_format'] = 'json'
default['praefect']['storage_nodes'] = []
